import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { EMPTY, of } from 'rxjs';
import 'rxjs/add/operator/delay';

import { AuthenticationService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { validationAllErrorMessagesService } from 'src/app/shared/validators/validatorService';
import { UserService } from 'src/app/services/user.services';
import { ProgressBarService } from 'src/app/shared/services/progress-bar.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    loginIOForm: FormGroup;
    errorLogin: string;
  
    // error message
    validationErrorMessages = {
      userName: { required: "Tên tài khoản không được để trống!" },
      passWord: { required: "Mật khẩu không được để trống!" }
    };
  
    // form errors
    formErrors = {
      userName: "",
      passWord: ""
    };
    constructor(
      private formBuilder: FormBuilder,
      public userService: UserService,
      private commonService: CommonServiceShared,
      public progressBarService: ProgressBarService
    ) { }
  
    ngOnInit() {
      this.formInit();
    }
  
    formInit() {
      this.loginIOForm = this.formBuilder.group({
        userName: ["", Validators.required],
        passWord: ["", Validators.required]
      });
    }
  
    // Sự kiện đăng nhập
    public loginAdmin() {
      this.logAllValidationErrorMessages();
      if (this.loginIOForm.valid === true) {
        const inputLogin = { userName: this.loginIOForm.value.userName, password: this.loginIOForm.value.passWord };
        this.userService.login(inputLogin).subscribe(
            (res:any)  => {
            localStorage.setItem('token', res.token);
            document.location.href = '/';
          },
          error => { 
            this.commonService.showeNotiResult(
              "Tên đăng nhập hoặc mật khẩu không chính xác",
              2000
            )}
        );
      }
    }
    // Validation click submit
    public logAllValidationErrorMessages() {
      validationAllErrorMessagesService(
        this.loginIOForm,
        this.validationErrorMessages,
        this.formErrors
      );
    }
}
