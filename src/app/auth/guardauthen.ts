import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { RoleConstants } from '../shared/constants';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    if (localStorage.getItem('token') !== null)
      return true;
    else {
      document.location.href = '/login';
      return false;
    }
  }
}

export class GiamDocGuard implements CanActivate {
  constructor() {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    
    let currentUser = JSON.parse(localStorage.getItem('currentAcc'));
    if (RoleConstants.NHOMONLYGIAMDOC.includes(currentUser.role) || RoleConstants.PHUTRACHTONGHOP == currentUser.role || RoleConstants.SU == currentUser.role)
      return true;
    else {
      document.location.href = '/admin';
      return false;
    }
  }
}
