
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminModule } from './admin/admin.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { RolelabelPipe } from './shared/pipes/role-label.pipe';
import { ProgressService } from './shared/services/progress-http.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    AdminModule,
    CoreModule
  ],

  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ProgressService,
      multi: true
    }
  ],
  entryComponents:[
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
