import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "GetNameByArrayIdPipe",
})
export class GetNameByArrayIdPipe implements PipeTransform {
    transform(value: any, nameTag: string, arrObject: any): string {
        let result = " ";
        if (arrObject && value) {
            // let arrayId = value.split(',');
            value.forEach(element => {
                for (const item of arrObject) {
                    if (item.id == element) {
                        result = result + item[nameTag] + " - " ;
                        break;
                    }
                }
            });

        }
        result = result.substring(0, result.length - 2);
        return result;
    }
}