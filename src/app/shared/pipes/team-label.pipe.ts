import { TeamService } from 'src/app/services/team.service';
import { RoleService } from './../../services/role.service';
import { Pipe, PipeTransform } from '@angular/core';
import { UserService } from 'src/app/services/user.services';

@Pipe({ name: 'TeamlabelPipe' })
export class TeamlabelPipe implements PipeTransform {
    /**
     *
     */
    constructor(private teamService: TeamService) { }
    async transform(id: any) {
        if(id) {
            var team = await this.teamService.getFetchById(id);

            return team.name;
        }
        return '';
    }
}