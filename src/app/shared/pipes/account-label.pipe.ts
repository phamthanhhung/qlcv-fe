import { RoleService } from './../../services/role.service';
import { Pipe, PipeTransform } from '@angular/core';
import { UserService } from 'src/app/services/user.services';

@Pipe({ name: 'AccountlabelPipe' })
export class AccountlabelPipe implements PipeTransform {
    /**
     *
     */
    constructor(private userService: UserService) { }
    async transform(id: any) {
        if(id) {
            var role = await this.userService.getAccountExtendById(id);

            return role.fullName;
        }
        return '';
    }
}