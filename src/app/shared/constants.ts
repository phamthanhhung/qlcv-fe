export const RoleConstants = {
    GIAMDOC: 'GĐ',
    PHOGIAMDOC: 'PGĐ',
    TRUONGPHONG: 'TP',
    PHOTRUONGPHONG: 'PTP',
    TOTRUONG: 'TT',
    NHANVIEN: 'NV',
    SU: 'SU',
    PHUTRACHTONGHOP: 'PTTH',

    NHOMROLEGIAMDOC : ["GĐ", "PGĐ", "SU", "KTT"],
    NHOMONLYGIAMDOC : ["GĐ", "PGĐ"],
    NHOMTRUONGPHONG : ["TP", "PTP", "PTTH"],
    NHOMCANEDITISSUES : ["TP", "PTTH","PTP"],
    NHOMROLENHANVIEN : ["NV", "TT"]
  };