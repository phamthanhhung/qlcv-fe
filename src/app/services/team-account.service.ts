import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BaseRepositoryService } from './baserepository.service';


@Injectable({
  providedIn: 'root'
})
export class TeamAccountService {

  serviceName = "team-account";

  constructor(private repository: BaseRepositoryService) {
  }


  // Lấy danh sách team-account
  public getAll(teamid: string) {
    var response = this.repository.getFetchRepository(this.serviceName + "/?teamId=" + teamid);
    return response;
  }

  // Lấy danh sách team-account by idAccount
  public getbyAccountId(accountId: string) {
    var response = this.repository.getFetchRepository(this.serviceName + "/accountId?accountId=" + accountId);
    return response;
  }

  // add team-account
  public insert(obj: any) {
    return this.repository.addRepository(this.serviceName, obj);
  }

  // Xóa team-account
  public delete(obj: any) {
    return this.repository.addRepository(this.serviceName + "/delete", obj);
  }

}
