import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BaseRepositoryService } from './baserepository.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  serviceName = "user";

  constructor(private repository: BaseRepositoryService) {
  }

  public login(obj: any) {

    return this.repository.addRepository(this.serviceName + "/login", obj);
  }

  // Lấy thông tin user
  public getProfile() {
    return this.repository.getRepository(this.serviceName + "/profile");
  }

  // Lấy thông tin user fetch
  public getFetchProfile() {
    var response = this.repository.getFetchRepository(this.serviceName + "/profile");
    return response;
  }

  // Lấy danh sách account
  public getAccount() {
    return this.repository.getRepository(this.serviceName + "/account");
  }

  // Lấy danh sách account fetch
  public getFetchAccount() {
    var response = this.repository.getFetchRepository(this.serviceName + "/account");
    return response;
  }

  // Lấy account hệ thống by id
  public getAccountById(id: string) {
    var response = this.repository.getFetchRepository(this.serviceName + "/get-by-id?id=" + id);
    return response;
  }
  // Lấy account hiển thị by id
  public getAccountExtendById(id: string) {
    var response = this.repository.getFetchRepository(this.serviceName + "/account-by-id?id=" + id);
    return response;
  }

  // Lấy account hiển thị by id fetch
  public getAccountExtendByUserId(userId: string) {
    var response = this.repository.getFetchRepository(this.serviceName + "/account-by-userid?userId=" + userId);
    return response;
  }
  // Lấy danh sách account
  public getAccountByRole(role) {
    var response = this.repository.getFetchRepository(this.serviceName + "/account?role=" + role);
    return response;
  }
  // Thêm mới user và account
  public insertUser(obj: any) {
    return this.repository.addRepository(this.serviceName + "/create-user", obj);
  }

  // Cập nhật user và account
  public updatetUser(obj: any) {
    return this.repository.updateRepository(this.serviceName, obj);
  }

  // delete user và account
  public deletetUser(id: string) {
    return this.repository.deleteRepository(this.serviceName + '?Id=' + id);
  }

  // Cập nhật user và account
  public updatetCurrentUserInfo(obj: any) {
    return this.repository.updateRepository(this.serviceName + "/currentUser", obj);
  }

  // Cập nhật current user password
  public updatetCurrentUserPassword(obj: any) {
    return this.repository.addRepository(this.serviceName + "/change-password", obj);
  }

  public resetPassword(userId: string) {
    return this.repository.getRepository(this.serviceName + "/reset-password?UserId=" + userId);
  }
}
