import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BaseRepositoryService } from './baserepository.service';


@Injectable({
    providedIn: 'root'
})
export class SettingService {

    serviceName = "setting";

    constructor(private repository: BaseRepositoryService) {
    }

    public getFetchSetting(settingType, settingKey) {
        var response = this.repository.getFetchRepository(this.serviceName + "?settingType=" + settingType +  "&settingKey=" + settingKey);
        return response;
    }
}
