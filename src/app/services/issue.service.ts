import { async } from '@angular/core/testing';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BaseRepositoryService } from './baserepository.service';


@Injectable({
    providedIn: 'root'
})
export class IssueService {

    serviceName = "issue";

    constructor(private repository: BaseRepositoryService) {
    }

    public updateDueDate(obj: any) {
        return this.repository.addRepository(this.serviceName + '/gia-han', obj);
    }
    // Lấy danh sách issue theo idproject
    public getByIdProject(idproject: string, type?: any, accountId?: any) {
        return this.repository.getRepository(this.serviceName + '?idProject=' + idproject + '&type=' + type + '&accountId=' + accountId);
    }

    // Thêm mới issue
    public insert(obj: any) {
        return this.repository.addRepository(this.serviceName, obj);
    }
    // Update issue
    public update(obj: any) {
        return this.repository.updateRepository(this.serviceName, obj);
    }
    // delete issue
    public delete(id: any) {
        return this.repository.deleteRepository(this.serviceName + '?issueId=' + id);
    }
    // Lấy danh sách issue theo id_account
    public getByIdAccount(idAccount: string, type: any) {
        return this.repository.getRepository(this.serviceName + '/issue-account?accountId=' + idAccount + '&type=' + type);
    }

    // Lấy danh sách subtask
    public async getSubtask(parentId: string) {
        return await this.repository.getFetchRepository(this.serviceName + '/get-subtask?parentId=' + parentId);
    }

    // Lấy danh sách hoạt động
    public async getAction(issueId: string) {
        return await this.repository.getFetchRepository(this.serviceName + '/get-action?issueId=' + issueId);
    }

    // Lấy danh sách subtask
    public async getComment(issueId: string) {
        return await this.repository.getFetchRepository(this.serviceName + '/get-comment?issueId=' + issueId);
    }

    // Thêm mới issue
    public postComment(obj: any) {
        return this.repository.addRepository(this.serviceName + '/comment', obj);
    }

    // filter issue theo idproject
    public filterIssue(idproject: string, type?: any, accountId?: any, timeFrom?: any, timeTo?: any, status?: any, isRisingWork?: any, pageSize: number = 0, pageNumber: number = 0, isConfirm = "") {
        return this.repository.getRepository(this.serviceName + '?idProject=' + idproject + '&type=' + type + '&accountId=' + accountId
            + '&timeFrom=' + timeFrom + '&timeTo=' + timeTo + '&status=' + status + '&isRisingwork=' + isRisingWork + '&pageSize=' + pageSize + '&pageNumber=' + pageNumber + '&isConfirm=' + isConfirm);
    }

    // filter issue theo id_account
    public filterByIdAccount(idAccount: string, type: any, timeFrom?: any, timeTo?: any, status?: any, isRisingWork?: any, pageSize: number = 0, pageNumber: number = 0, isConfirm = "") {
        return this.repository.getRepository(this.serviceName + '/issue-account?accountId=' + idAccount + '&type=' + type + '&timeFrom=' +
            timeFrom + '&timeTo=' + timeTo + '&status=' + status + '&isRisingwork=' + isRisingWork + '&pageSize=' + pageSize + '&pageNumber=' + pageNumber + '&isConfirm=' + isConfirm);
    }

    // Lấy danh sách report by id project
    public getReportByIdProject(projectId: string, timeFrom?: any, timeTo?: any) {
        return this.repository.getRepository(this.serviceName + '/baocao?projectId=' + projectId + '&timeFrom=' + timeFrom + '&timeTo=' + timeTo);
    }

    // Lấy danh sách report by team
    public getReportByTeam(teamId: string, accountId:any, timeFrom?: any, timeTo?: any) {
        return this.repository.getRepository(this.serviceName + '/baocao-team?teamId=' + teamId + '&accountId=' + accountId +'&timeFrom=' + timeFrom + '&timeTo=' + timeTo);
    }
    // Lấy danh sách báo cáo cá nhân
    public getReportCanhan(accountId: string, timeFrom?: any, timeTo?: any) {
        return this.repository.getRepository(this.serviceName + '/baocao-canhan?accountId=' + accountId + '&timeFrom=' + timeFrom + '&timeTo=' + timeTo);
    }

    // Lấy danh sách báo cáo công việc
    public getReportCongviec(teamId: string, timeFrom?: any, timeTo?: any) {
        return this.repository.getRepository(this.serviceName + '/baocao-congviec?teamId=' + teamId + '&timeFrom=' + timeFrom + '&timeTo=' + timeTo);
    }

    // count công việc quá hạn và chưa hoàn thành
    public async getCountQuahanChuahoanthanh(accountId: string) {
        return await this.repository.getFetchRepository(this.serviceName + '/count?accountId=' + accountId);
    }

    // count công việc trong dashboard
    public async getCountDashboard(accountId: string) {
        return await this.repository.getFetchRepository(this.serviceName + '/count-dashboard?accountId=' + accountId);
    }

    public uploadFile(file: File[]) {
        return this.repository.uploadAndProgress(this.serviceName + '/upload', file);
    }
}
