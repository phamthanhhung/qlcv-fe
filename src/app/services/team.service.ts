import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BaseRepositoryService } from './baserepository.service';


@Injectable({
  providedIn: 'root'
})
export class TeamService {

  serviceName = "team";

  constructor(private repository: BaseRepositoryService) {
  }


  // Lấy danh sách team
  public getAll() {
    return this.repository.getRepository(this.serviceName);
  }

  // Thêm mới team
  public insert(obj: any) {
    return this.repository.addRepository(this.serviceName, obj);
  }

  // Cập nhật team
  public updatet(obj: any) {
    return this.repository.updateRepository(this.serviceName, obj);
  }

  // delete team
  public delete(id: string) {
    return this.repository.deleteRepository(this.serviceName + '?Id=' + id);

  }

  // Lấy danh sách team fetch
  public  getFetch() {
    var json =  this.repository.getFetchRepository(this.serviceName);
    return json ;
  }

  // Lấy team fetch by id
  public  getFetchById(id: string) {
    var json =  this.repository.getFetchRepository(this.serviceName + "/get-by-id?id=" + id);
    return json ;
  }

  // Lấy team by idleader
  public  getFetchByLeader(id: string) {
    var json =  this.repository.getFetchRepository(this.serviceName + "/get-by-leader?id=" + id);
    return json ;
  }
}
