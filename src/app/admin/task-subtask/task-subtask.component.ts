import { RoleConstants } from 'src/app/shared/constants';
import { TeamService } from './../../services/team.service';
import { SettingService } from './../../services/setting.service';
import { async } from '@angular/core/testing';
import { UserService } from './../../services/user.services';
import { CommonServiceShared } from './../../shared/services/common-services';
import { TaskIoComponent } from './task-io/task-io.component';
import { IssueService } from './../../services/issue.service';
import { ProjectService } from 'src/app/services/project.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { HttpErrorResponse } from '@angular/common/http';
import { DetailTaskComponent } from '../detail-task/detail-task.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-task-subtask',
  templateUrl: './task-subtask.component.html',
  styleUrls: ['./task-subtask.component.css']
})
export class TaskSubtaskComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'reporter_id', 'title', 'assign_id', 'due_date', 'confirm_date', 'action'];
  dataSource: any;

  listTeam: any = [];
  // dialog
  public mDialog: any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public idProject: any;
  public project: any = {};
  public listIssue: any = [];
  public listAccount: any = [];
  public currentUser: any;
  public listTrangthai: any;
  public listAction: any;
  // các biến lọc
  public trangthai: any = "";
  public timeFrom: any;
  public timeTo: any;

  public RoleConstants = RoleConstants;

  constructor(private router: Router, private projectService: ProjectService,
    private issueService: IssueService,
    private imDialog: MatDialog,
    public imDialogService: MatdialogService,
    public commonService: CommonServiceShared,
    public userService: UserService,
    public settingService: SettingService,
    public teamService: TeamService,
    public datePipe: DatePipe) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  async ngOnInit() {
    this.idProject = this.router.url.split('=')[1];
    await this.getProject();
    await this.getProfile();
    await this.getAction();
    // await this.getIssue();
    await this.getListAccount();
    await this.getListTrangthai();
    await this.getTeam();
    await this.filter();
    // await this.getTrangthai();
  }

  async getAction() {
    this.listAction = await this.projectService.getAction(this.idProject);
  }

  // check hiển thị nút xóa; các việc đã hoàn thành không được sửa xóa
  public checkDeleteButton(element) {
    if(element.status == 0) {
      return true;
    }
    if (RoleConstants.NHOMROLEGIAMDOC.includes(this.currentUser.role)) {
      return false;
    }
    else {
      if (element.idCreator == this.currentUser.id) {
        return false;
      }
      return true;
    }
  }

  // Hiển thị nút sửa với role GĐ TP; các việc đã hoàn thành không được sửa xóa
  public checkEditButton(element) {
    if(element.status == 0) {
      return true;
    }
    if (RoleConstants.NHOMROLEGIAMDOC.includes(this.currentUser.role)) {
      return false;
    }
    else {
      if (RoleConstants.NHOMTRUONGPHONG.includes(this.currentUser.role) && element.assignId == this.currentUser.id) {
        return false;
      }
      return true;
    }
  }

  // Hàm filter dữ liệu
  filter() {
    let dateFrom = "";
    let dateTo = "";

    if (this.timeFrom && this.timeTo) {
      dateFrom = this.datePipe.transform(
        this.timeFrom,
        "MM-dd-yyyy"
      );

      dateTo = this.datePipe.transform(
        this.timeTo,
        "MM-dd-yyyy"
      );

    }

    if (!RoleConstants.NHOMROLEGIAMDOC.includes(this.currentUser.role)) {
      this.issueService.filterIssue(this.idProject, "", this.currentUser.id, dateFrom, dateTo, this.trangthai, '').subscribe(res => {
        let stt = 0;
        var data = res.items.map((item) => {
          if (!item.parentId) {
            item.stt = stt + 1;
            stt++;
          }
        });
        this.listIssue = res.items;
        this.dataSource = new MatTableDataSource(this.listIssue);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
    } else {
      this.issueService.filterIssue(this.idProject, "", "", dateFrom, dateTo, this.trangthai, '').subscribe(res => {
        let stt = 0;
        var data = res.items.map((item) => {
          if (!item.parentId) {
            item.stt = stt + 1;
            stt++;
          }
        });
        this.listIssue = res.items;
        this.dataSource = new MatTableDataSource(this.listIssue);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
    }
  }


  async getProject() {
    this.projectService.getProjectById(this.idProject).subscribe(res => {
      this.project = res
    });

  }
  // lấy user hiện tại
  public async getProfile() {
    this.currentUser = await this.userService.getFetchProfile();
  }

  // async getIssue() {
  //   if (!RoleConstants.NHOMROLEGIAMDOC.includes(this.currentUser.role)) {
  //     this.issueService.getByIdProject(this.idProject, "", this.currentUser.id).subscribe(res => {
  //       var data = res.map((item, index) => {
  //         item.stt = index + 1;
  //       });
  //       this.listIssue = res;
  //       this.dataSource = new MatTableDataSource(this.listIssue);
  //       this.dataSource.sort = this.sort;
  //       this.dataSource.paginator = this.paginator;
  //     });
  //   } else {
  //     this.issueService.getByIdProject(this.idProject, "", "").subscribe(res => {
  //       var data = res.map((item, index) => {
  //         item.stt = index + 1;
  //       });
  //       this.listIssue = res;
  //       this.dataSource = new MatTableDataSource(this.listIssue);
  //       this.dataSource.sort = this.sort;
  //       this.dataSource.paginator = this.paginator;
  //     });
  //   }

  // };
  public async getTeam() {
    this.listTeam = await this.teamService.getFetch();
  }

  public async getListAccount() {
    this.listAccount = await this.userService.getFetchAccount();
  }

  public async getListTrangthai() {
    this.listTrangthai = await this.settingService.getFetchSetting('0', 'trangthai');
  }
  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
  view(item) {

    this.mDialog.setDialog(
      this,
      DetailTaskComponent,
      "",
      "",
      item,
      "60%",
      "90vh",
      1
    );
    this.mDialog.open();
  }
  addTask() {
    var data = {
      purpose: "add",
      idProject: this.idProject,
      idLeader: this.project.idLeader,
      type: this.project.type
    }
    this.showPopup(data);
  }

  editTeam(item) {
    item.purpose = "edit";
    item.idProject = this.idProject;
    item.idLeader = this.project.idLeader
    item.type = this.project.type
    this.showPopup(item);
  }

  // thêm subtask
  addSubTask(item) {
    var data = {
      purpose: "add",
      idProject: item.idProject,
      type: this.project.type,
      addSubtask: true,
      parentId: item.id
    }
    this.showPopup(data);
  }

  deleteTeam(item) {
    const dialogRef = this.commonService.confirmDeleteDiaLogService(
      "Tên công việc:",
      item.title
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.issueService
          .delete(item.id).subscribe(
            () => this.filter(),
            (error: HttpErrorResponse) => {
              this.commonService.showError(error);
            },
            () => this.commonService.showeNotiResult("Đã xóa thành công: " + item.title, 2000)
          );
      }
    });
  }
  /**
 * Mở popup io
 */
  public showPopup(data?) {

    this.mDialog.setDialog(
      this,
      TaskIoComponent,
      "",
      "",
      data,
      "50%",
      "60vh",
      1
    );
    this.mDialog.open();
  }

  /**
 * Hàm đóng mat dialog
 */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  doFunction(methodName) {
    this[methodName]();
  }
}
