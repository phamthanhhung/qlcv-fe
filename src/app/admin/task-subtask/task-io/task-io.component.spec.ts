import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskIoComponent } from './task-io.component';

describe('TaskIoComponent', () => {
  let component: TaskIoComponent;
  let fixture: ComponentFixture<TaskIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
