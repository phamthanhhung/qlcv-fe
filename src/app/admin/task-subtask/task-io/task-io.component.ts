import { RoleConstants } from 'src/app/shared/constants';
import { SettingService } from './../../../services/setting.service';
import { UserService } from 'src/app/services/user.services';
import { ProjectService } from 'src/app/services/project.service';
import { TeamService } from 'src/app/services/team.service';
import { IssueService } from './../../../services/issue.service';
import { HttpErrorResponse, HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RoleService } from 'src/app/services/role.service';
import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { ProgressService } from 'src/app/shared/services/progress-http.service';
import { validationAllErrorMessagesService, displayFieldCssService } from 'src/app/shared/validators/validatorService';
import { RoleIoComponent } from '../../roles/role-io/role-io.component';
import { TeamAccountService } from 'src/app/services/team-account.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-task-io',
  templateUrl: './task-io.component.html',
  styleUrls: ['./task-io.component.css']
})
export class TaskIoComponent implements OnInit {
  RoleConstants = RoleConstants;
  IOForm: FormGroup;
  public inputModel: any;
  public editMode: boolean;
  public purpose: string;
  public obj: any;
  dataModel: any;
  listDuan: any;
  listUser: any;
  listNguoigiaoviec: any;
  listNguoinhanviec: any;
  listTrangthai: any;
  listMucdo: any;
  listTeam: any;
  public currentUser: any = {};

  projectDueDate: any;
  checkIssueDueDate: boolean = true;

  percentDone = 0;
  uploadSuccess = false;

  public fileUrl: any = {
    url: ""
  };
  // error message
  validationErrorMessages = {
    title: { required: "Trường bắt buộc nhập!" },
    idProject: { required: "Trường bắt buộc nhập!" },
    assignId: { required: "Trường bắt buộc nhập!" },
    dueDate: { required: "Trường bắt buộc nhập!" },
    priority: { required: "Trường bắt buộc nhập!" },
    reporterId: { required: "Trường bắt buộc nhập!" },
    status: { required: "Trường bắt buộc nhập!" },
    startedDate: { required: "Trường bắt buộc nhập!" },
  };

  // form errors
  formErrors = {
    title: "",
    idProject: "",
    assignId: "",
    dueDate: "",
    priority: "",
    reporterId: "",
    status: "",
    startedDate: ""
  };

  public checkDisableTittle = false;
  // ctor
  constructor(
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public imDialogService: MatdialogService,
    private projectService: ProjectService,
    private commonService: CommonServiceShared,
    public dialogRef: MatDialogRef<RoleIoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public issueService: IssueService,
    public userService: UserService,
    public settingService: SettingService,
    public teamAccountService: TeamAccountService,
    public teamService: TeamService
  ) { }

  // onInit
  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentAcc'));
    if (this.data.model && this.data.model.purpose) {
      this.purpose = this.data.model.purpose;
      this.dataModel = this.data.model
    }

    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }
  // lấy json trạng thái
  async getTrangthai() {
    this.listTrangthai = await this.settingService.getFetchSetting('0', 'trangthai');
  }

  async getMucdo() {
    this.listMucdo = await this.settingService.getFetchSetting('0', 'priority');
  }

  async getTeam() {
    this.listTeam = await this.teamService.getFetch();
  }
  // Các hàm lấy data
  public async initData() {
    await this.getTrangthai();
    await this.getMucdo();
    await this.getProfile();
    await this.getDuan();
    await this.getTeam();
    await this.getUserList();
    await this.getUserListByProject();
  }
  // config Form use add or update
  bindingConfigAddOrUpdate() {
    if (this.dataModel && this.purpose === "edit") {
      this.initData();
      this.checkDisableTittle = this.checkEditName(this.dataModel);
      this.IOForm.setValue({
        title: this.dataModel.title,
        description: this.dataModel.description,
        idProject: this.dataModel.idProject,
        priority: this.dataModel.priority,
        reporterId: this.dataModel.reporterId,
        assignId: this.dataModel.assignId,
        coopId: this.dataModel.coopId,
        status: this.dataModel.status,
        startedDate: this.dataModel.startedDate,
        dueDate: this.dataModel.dueDate,
        isRisingwork: this.dataModel.isRisingwork,
        url: this.dataModel.url,
        idTeam: this.dataModel.idTeam
      });
      this.fileUrl.url = this.dataModel.fileUrl;
    }
  }

  // config input validation form
  bindingConfigValidation() {
    this.initData();

    this.IOForm = this.formBuilder.group({
      title: ["", Validators.required],
      description: [""],
      idProject: ["", Validators.required],
      priority: [1, Validators.required],
      reporterId: ["", Validators.required],
      assignId: ["", Validators.required],
      coopId: [""],
      status: [1, Validators.required],
      startedDate: [new Date()],
      dueDate: [""],
      isRisingwork: [false],
      url: [""],
      idTeam: [""]
    });

    if (this.purpose === 'add') {
      this.IOForm.controls["idProject"].setValue(this.dataModel.idProject);
    }
    if (RoleConstants.NHOMROLENHANVIEN.includes(this.currentUser.role)) {
      this.IOForm.controls["isRisingwork"].setValue(true);
    }

  }

  // on Submit
  public onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    this.checkIssueDueDate = this.checkDueDate(this.projectDueDate, this.IOForm.value.dueDate);
    if (this.IOForm.valid === true && this.purpose === 'add' && this.checkIssueDueDate == true) {
      this.inputModel = this.IOForm.value;
      this.inputModel.idType = this.dataModel.type;
      this.inputModel.userActionId = JSON.parse(localStorage.getItem('currentAcc')).id;
      this.inputModel.idCreator = JSON.parse(localStorage.getItem('currentAcc')).id;

      if (this.dataModel.addSubtask === true) {
        this.inputModel.parentId = this.dataModel.parentId;
      }
      this.inputModel.fileUrl = this.fileUrl.url;
      this.inputModel.creatorUserRole = this.currentUser.role;
      this.issueService.insert(this.inputModel).subscribe(
        res => this.imDialogService.doParentFunction("filter"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Thêm mới công việc thành công!",
            2000
          )
      );
      this.imDialogService.doParentFunction("closeMatDialog");
    } if (this.IOForm.valid === true && this.purpose === 'edit' && this.checkIssueDueDate == true) {
      this.inputModel = this.IOForm.value;
      this.inputModel.id = this.dataModel.id;
      this.inputModel.idType = this.dataModel.type;
      this.inputModel.parentId = this.dataModel.parentId;
      this.inputModel.userActionId = JSON.parse(localStorage.getItem('currentAcc')).id;

      this.inputModel.fileUrl = this.fileUrl.url;
      // khi trạng thái là đổi sang hoàn thành, tự động đổi ngày hoàn thành là ngày hiện tại
      if (this.inputModel.status == 0 && this.dataModel.status != 0) {
        this.inputModel.confirmDate = new Date();
      }
      this.issueService.update(this.inputModel).subscribe(
        res => this.imDialogService.doParentFunction("filter"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Cập nhật công việc thành công!",
            2000
          )
      );
      this.imDialogService.doParentFunction("closeMatDialog");
    }
  }

  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.IOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // lấy danh sách dự án
  public async getDuan() {
    this.listDuan = await this.projectService.getProjectds();
  }

  // lấy user hiện tại
  public async getProfile() {
    this.currentUser = await this.userService.getFetchProfile();
  }

  // lấy danh sách user
  public async getUserList() {
    this.listUser = await this.userService.getFetchAccount();
  }
  // lấy danh sách user theo dự án
  public async getUserListByProject() {
    var idProject = this.dataModel.idProject;
    let project = (this.listDuan.filter(a => a.id == idProject))[0];
    this.listNguoinhanviec = await this.teamAccountService.getAll(project.idTeam);

    this.listNguoigiaoviec = Object.create(this.listNguoinhanviec);

    let giamdoc = this.listUser.filter(a => RoleConstants.NHOMONLYGIAMDOC.includes(a.role));
    giamdoc.forEach(element => {
      let temp = {
        accountId: element.id,
        accountName: element.fullName
      }
      this.listNguoigiaoviec.push(temp);
    });
    this.projectDueDate = project.end;
    this.checkIssueDueDate = this.checkDueDate(this.projectDueDate, this.IOForm.value.dueDate);
    // idTeam theo nhiệm vụ
    this.IOForm.controls["idTeam"].setValue(project.idTeam);
    if (this.purpose == 'add') {
      // gán current user là người giao việc
      this.IOForm.controls["reporterId"].setValue(this.currentUser.id);

      if (RoleConstants.NHOMROLEGIAMDOC.includes(this.currentUser.role)) {
        let team = (this.listTeam.filter(a => a.id == project.idTeam))[0];
        this.IOForm.controls["assignId"].setValue(team.idLeader);
      }
      this.IOForm.controls["dueDate"].setValue(project.end);
    }

  }

  // Kiểm tra dueDate công việc so với dueDate nhiệm vụ
  public checkDueDate(projectDueDate, issueDueDate) {
    if (!projectDueDate || !issueDueDate) {
      return true;
    } else {
      projectDueDate = formatDate(projectDueDate, 'yyyy-MM-dd', 'en_US');

      issueDueDate = formatDate(issueDueDate, 'yyyy-MM-dd', 'en_US');

      if (projectDueDate >= issueDueDate) {
        return true;
      } else {
        return false;
      }
    }
  }
  // close sidebar
  public closeIOSidebar() {
    this.imDialogService.doParentFunction("closeMatDialog");
  }
  // xử lí upload file
  uploadFile(files: File[]) {
    this.percentDone = 0;
    this.issueService.uploadFile(files).subscribe(
      event => {

        if (event.type === HttpEventType.UploadProgress) {
          this.percentDone = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.fileUrl = event.body;
          this.uploadSuccess = true;
        }
      },
      (error: HttpErrorResponse) => {
        this.commonService.showeNotiResult(error.message, 2000);
      }
    )
  }

  // Xóa ngày hết hạn
  public clearStartDate() {
    this.IOForm.controls['startedDate'].setValue("");
  }

  // Không cho phép chỉnh sửa tên công việc
  public checkEditName(element) {
    if (RoleConstants.NHOMROLEGIAMDOC.includes(this.currentUser.role)) {
      return false;
    }
    else {
      if (element.idCreator == this.currentUser.id) {
        return false;
      }
      return true;
    }
  }

}
