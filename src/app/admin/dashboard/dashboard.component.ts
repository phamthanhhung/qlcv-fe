import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { IssueService } from 'src/app/services/issue.service';
import { UserService } from 'src/app/services/user.services';
import { RoleConstants } from 'src/app/shared/constants';
import { CommonServiceShared } from 'src/app/shared/services/common-services';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public currentAccount: any;
  public countDashboard: any;
  public chartData = [];
  public chart: any;
  constructor(
    private issueService: IssueService,
    private commonService: CommonServiceShared,
    private userService: UserService
  ) { }

  async ngOnInit() {
    await this.getInformationUser();
    await this.getCountDashboard();
    this.initChart();
  }

  // Lấy thông tin người dùng
  async getInformationUser() {

    this.currentAccount = await this.userService.getFetchProfile();
    this.commonService.showeNotiResult(`Xin chào, ${this.currentAccount.fullName}`, 3000);
  }

  public initChart() {
    this.chart = new Chart({
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      credits: {
        enabled: false
      },
      title: {
        text: 'Biểu đồ thống kê công việc'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
      },
      accessibility: {
        point: {
          valueSuffix: '%'
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          colors: [
            '#24CBE5',
            '#64E572',
            '#FF9655',
            '#FFF263',
            '#6AF9C4'
          ],
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
          }
        }
      },
      series: [{
        type: undefined,
        name: 'Số lượng',
        colorByPoint: true,
        data: this.chartData
      }]
    });
  }


  public async getCountDashboard() {

    if (RoleConstants.NHOMROLEGIAMDOC.includes(this.currentAccount.role)) {
      this.countDashboard = await this.issueService.getCountQuahanChuahoanthanh("");
    } else {
      this.countDashboard = await this.issueService.getCountQuahanChuahoanthanh(this.currentAccount.id);
    }

    this.chartData = [{
      name: 'Công việc quá hạn',
      y: this.countDashboard.countQuaHan
    },
    {
      name: 'Công việc chưa hoàn thành',
      y: this.countDashboard.countChuahoanthanh
    },
    {
      name: 'Công việc đã hoàn thành',
      y: this.countDashboard.countHoanthanh
    },
    {
      name: 'Công việc đã hoàn thành quá hạn',
      y: this.countDashboard.countHoanThanhQuaHan
    },
    ]
  }
}
