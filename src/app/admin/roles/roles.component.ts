import { RoleService } from './../../services/role.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { UserService } from 'src/app/services/user.services';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { RoleIoComponent } from './role-io/role-io.component';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'roleName', 'roleLabel', 'action'];
  dataSource: any;

  listRole: any = [];
  // dialog
  public mDialog: any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private roleService: RoleService,
    private imDialog: MatDialog,
    public imDialogService: MatdialogService,
  ) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  async ngOnInit() {
    await this.getRole();


  }
  async getRole() {
    this.roleService.getRole().subscribe(res => {

      var data = res.map((item, index) => {
        item.stt = index + 1;
      });
      this.listRole = res;
      this.dataSource = new MatTableDataSource(this.listRole);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  addRole() {
    this.showPopup();
  }
  editRole(item) {
    item.purpose = "edit";
    this.showPopup(item);
  }
  deleteRole() {

  }

  public doFilter = (value: string) => {

    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  /**
   * Mở popup
   */
  public showPopup(data?) {

    this.mDialog.setDialog(
      this,
      RoleIoComponent,
      "",
      "",
      data,
      "40%",
      "25vh",
      1
    );
    this.mDialog.open();
  }

  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  doFunction(methodName) {
    this[methodName]();
  }

}
