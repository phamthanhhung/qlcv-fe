import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleIoComponent } from './role-io.component';

describe('RoleIoComponent', () => {
  let component: RoleIoComponent;
  let fixture: ComponentFixture<RoleIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
