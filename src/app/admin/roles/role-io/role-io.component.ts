import { RoleService } from './../../../services/role.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from 'src/app/services/user.services';
import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { ProgressService } from 'src/app/shared/services/progress-http.service';
import { validationAllErrorMessagesService, displayFieldCssService } from 'src/app/shared/validators/validatorService';

@Component({
  selector: 'app-role-io',
  templateUrl: './role-io.component.html',
  styleUrls: ['./role-io.component.css']
})
export class RoleIoComponent implements OnInit {


  IOForm: FormGroup;
  public inputModel: any;
  public editMode: boolean;
  public purpose: string;
  public obj: any;
  dataModel: any;
  // error message
  validationErrorMessages = {
    roleName: { required: "Mã vai trò không được để trống!" },
    roleLabel: { required: "Tên vai trò không được để trống!" },
  };

  // form errors
  formErrors = {
    roleName: "",
    roleLabel: "",
  };

  // ctor
  constructor(
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public imDialogService: MatdialogService,
    private roleService: RoleService,
    private commonService: CommonServiceShared,
    public dialogRef: MatDialogRef<RoleIoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,

  ) { }

  // onInit
  ngOnInit() {
    if (this.data.model && this.data.model.purpose) {
      this.purpose = this.data.model.purpose;
      this.dataModel = this.data.model
    }
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }

  // config Form use add or update
  bindingConfigAddOrUpdate() {
    // check edit
    if (this.dataModel) {
      this.IOForm.setValue({
        roleName: this.dataModel.roleName,
        // password: this.dataModel.password,
        roleLabel: this.dataModel.roleLabel,
      });
    }
  }

  // config input validation form
  bindingConfigValidation() {

    this.IOForm = this.formBuilder.group({
      roleName: ["", Validators.required],
      roleLabel: ["", Validators.required],
    });
  }


  // on Submit
  public onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.IOForm.valid === true && !this.purpose) {
      this.inputModel = this.IOForm.value;
      this.roleService.insertRole(this.inputModel).subscribe(
        res => this.imDialogService.doParentFunction("getRole"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Thêm mới vai trò thành công!",
            2000
          )
      );
      this.imDialogService.doParentFunction("closeMatDialog");
    } if (this.IOForm.valid === true && this.purpose) {
      this.inputModel = this.IOForm.value;
      this.inputModel.id = this.dataModel.id;
      this.roleService.updateRole(this.inputModel).subscribe(
        res => this.imDialogService.doParentFunction("getRole"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Cập nhật vai trò thành công!",
            2000
          )
      );
      this.imDialogService.doParentFunction("closeMatDialog");
    }


  }

  // add or update continue
  public addOrUpdateContinue(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.IOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }

  // add or update
  private addOrUpdate(operMode: string) {

  }

  // on form reset
  public onFormReset() {
    this.IOForm.reset();
  }
  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.IOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // display fields css
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  // close sidebar
  public closeIOSidebar() {
    this.imDialogService.doParentFunction("closeMatDialog");
  }

}
