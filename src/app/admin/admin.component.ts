import { MediaMatcher } from '@angular/cdk/layout';
import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthGuard } from '../auth/guardauthen';
import { AuthenticationService } from '../core/services/auth.service';
import { SpinnerService } from '../core/services/spinner.service';
import { IssueService } from '../services/issue.service';
import { UserService } from '../services/user.services';
import { RoleConstants } from '../shared/constants';
import { ProgressBarService } from '../shared/services/progress-bar.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-root',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit, OnDestroy, AfterViewInit {

  private _mobileQueryListener: () => void;
  mobileQuery: MediaQueryList;
  showSpinner: boolean;
  userName: string;
  isAdmin: boolean;
  user: any = {};
  countQuahanChuahoanthanh: any;

  public checkRole = false;

  RoleConstants = RoleConstants;

  constructor(private changeDetectorRef: ChangeDetectorRef,
    private media: MediaMatcher,
    private issueService: IssueService,
    private userService: UserService,
    public progressBarService: ProgressBarService) {

    this.mobileQuery = this.media.matchMedia('(max-width: 1000px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    // tslint:disable-next-line: deprecation
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  async ngOnInit() {
    await this.getInformationUser();
  }

  ngOnDestroy(): void {
    // tslint:disable-next-line: deprecation
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  ngAfterViewInit(): void {
    this.changeDetectorRef.detectChanges();
  }
  ngAfterContentChecked() {
    this.changeDetectorRef.detectChanges();
  }
  // Lấy thông tin người dùng
  async getInformationUser() {
    this.userService.getProfile().subscribe(res => {
      this.user = res;
      localStorage.setItem('currentAcc', JSON.stringify(res));
      if (RoleConstants.NHOMONLYGIAMDOC.includes(res.role) || RoleConstants.PHUTRACHTONGHOP == res.role || RoleConstants.SU == res.role) {
        this.checkRole = true;
      }
      this.getCountQuahanChuahoanthanh();
    });
  }

  public async getCountQuahanChuahoanthanh() {
    if (RoleConstants.NHOMROLEGIAMDOC.includes(this.user.role)) {
      this.countQuahanChuahoanthanh = await this.issueService.getCountQuahanChuahoanthanh("");
    } else {
      this.countQuahanChuahoanthanh = await this.issueService.getCountQuahanChuahoanthanh(this.user.id);
    }

  }
  // Logout khỏi hệ thống
  public logoutAdmin() {
    localStorage.removeItem('currentAcc');
    localStorage.removeItem('token');
    document.location.href = '/login';
  }

}
