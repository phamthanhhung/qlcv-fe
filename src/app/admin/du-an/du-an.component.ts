import { RoleConstants } from 'src/app/shared/constants';
import { UserService } from './../../services/user.services';
import { async } from '@angular/core/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { ProjectService } from 'src/app/services/project.service';
import { TeamService } from 'src/app/services/team.service';
import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { NvChuyenmonIoComponent } from '../nv-chuyenmon/nv-chuyenmon-io/nv-chuyenmon-io.component';
import { TeamMemberComponent } from '../teams/team-member/team-member.component';
import { DuanIoComponent } from './duan-io/duan-io.component';

@Component({
  selector: 'app-du-an',
  templateUrl: './du-an.component.html',
  styleUrls: ['./du-an.component.css']
})
export class DuAnComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'name', 'start', 'end', 'team', 'leader', 'action'];
  dataSource: any;
  listDuan: any = [];

  listTeam:any = [];
  listAccount:any = [];
  listGiamdoc:any = [];
  // dialog
  public mDialog: any;
  public currentAccount:any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  item = [
    {
      key: 'Đang mở',
      value: 0
    },
    {
      key: 'Đang tiến hàng',
      value: 1
    },
    {
      key: 'Hoàn thành',
      value: 2
    }

  ]
  constructor(
    private imDialog: MatDialog,
    public imDialogService: MatdialogService,
    public commonService: CommonServiceShared,
    public projectService: ProjectService,
    private router: Router,
    private teamService: TeamService,
    private userService: UserService
  ) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  async ngOnInit() {
    this.currentAccount = JSON.parse(localStorage.getItem('currentAcc'));
    await this.getDuan();
    await this.getTeam();
    await this.getListAccount();

  }

  async getDuan() {
    if(!RoleConstants.NHOMROLEGIAMDOC.includes(this.currentAccount.role)) {
      this.projectService.getDuan(this.currentAccount.id).subscribe(res => {

        var data = res.map((item, index) => {
          item.stt = index + 1;
        });
        this.listDuan = res;
        this.dataSource = new MatTableDataSource(this.listDuan);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
    } else {
      this.projectService.getDuan("").subscribe(res => {

        var data = res.map((item, index) => {
          item.stt = index + 1;
        });
        this.listDuan = res;
        this.dataSource = new MatTableDataSource(this.listDuan);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
    }

  }

  public async getTeam() {
    this.listTeam = await this.teamService.getFetch();
  }

  public async getListAccount() {
    this.listAccount = await this.userService.getFetchAccount();
    this.listGiamdoc = this.listAccount.filter(a => RoleConstants.NHOMONLYGIAMDOC.includes(a.role));
  }

  addTeam() {
    this.showPopup();
  }

  editTeam(item) {
    item.purpose = "edit";
    this.showPopup(item);
  }

  deleteTeam(item) {
    const dialogRef = this.commonService.confirmDeleteDiaLogService(
      "Tên nhiệm vụ:",
      item.name
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.projectService
          .delete(item.id).subscribe(
            () => this.getDuan(),
            (error: HttpErrorResponse) => {
              this.commonService.showError(error);
            },
            () => this.commonService.showeNotiResult("Đã xóa thành công: " + item.name, 2000)
          );
      }
    });
  }

  addTask(item) {
    this.router.navigateByUrl('/admin/task?projectid=' + item.id);
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  /**
   * Mở popup io
   */
  public showPopup(data?) {
    this.mDialog.setDialog(
      this,
      DuanIoComponent,
      "",
      "",
      data,
      "50%",
      "45vh",
      1
    );
    this.mDialog.open();
  }

  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  doFunction(methodName) {
    this[methodName]();
  }


}
