import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamIoComponent } from './team-io.component';

describe('TeamIoComponent', () => {
  let component: TeamIoComponent;
  let fixture: ComponentFixture<TeamIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
