import { async } from '@angular/core/testing';
import { TeamAccountService } from './../../../services/team-account.service';
import { SelectionModel } from '@angular/cdk/collections';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import { RoleService } from 'src/app/services/role.service';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.services';
import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { TeamIoComponent } from '../team-io/team-io.component';

@Component({
  selector: 'app-team-member',
  templateUrl: './team-member.component.html',
  styleUrls: ['./team-member.component.css']
})
export class TeamMemberComponent implements OnInit {
  selection = new SelectionModel<any>(true, []);
  displayedColumns: string[] = ['stt', 'fullName', 'sodienthoai', 'email', 'role', 'select'];
  dataSource: any;

  listMember: any = [];

  listTeamAccount: any = [];
  listIdAccount = [];
  dataModel: any;
  listRole: any = [];
  // danh sách thành viên mới đc chọn
  checkedList: any = [];

  // danh sách thành viên bị remove
  removeList: any = [];
  // dialog
  public mDialog: any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private userService: UserService,
    private imDialog: MatDialog,
    public imDialogService: MatdialogService,
    private roleService: RoleService,
    private commonService: CommonServiceShared,
    private teamAccService: TeamAccountService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  async ngOnInit() {
    if (this.data.model) {
      this.dataModel = this.data.model
    }

    await this.getMember();

    await this.getTeamAccount();
    await this.getRole();

  }
  async getMember() {
    this.userService.getAccount().subscribe(res => {

      var data = res.map(async (item, index) => {
        item.stt = index + 1;
      });
      this.listMember = res;
      this.dataSource = new MatTableDataSource(this.listMember);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  public async getRole() {
    this.listRole = await this.roleService.getFetchRole();
  }
  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  checkboxchangeAdd(event, data) {
    const target = event;
    if (target.checked === true) {
      this.checkedList.push(data.id);
    }
    if (target.checked === false) {
      this.checkedList.splice(this.checkedList.indexOf(data.id), 1);
    }

  }

  checkboxchangeRemove(event, data) {
    const target = event;
    if (target.checked === false) {
      this.removeList.push(data.id);
    }
    if (target.checked === true) {
      this.removeList.splice(this.removeList.indexOf(data.id), 1);
    }

  }

  addMember() {
    // gọi hàm xóa các member từ removeList
    var removeModel = {
      idTeam: this.dataModel.id,
      list: this.removeList
    }

    this.teamAccService.delete(removeModel).subscribe(
      () => this.commonService.showeNotiResult("Đã cập nhật thành công!", 2000)
    );
    // gọi hàm thêm mới member từ checkList
    var addModel = {
      idTeam: this.dataModel.id,
      list: this.checkedList
    }

    this.teamAccService.insert(addModel).subscribe(
      () => this.commonService.showeNotiResult("Đã cập nhật thành công!", 2000)
    );
    this.closeMatDialog();
  }
  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  doFunction(methodName) {
    this[methodName]();
  }

  async getTeamAccount() {
    this.listTeamAccount = await this.teamAccService.getAll(this.dataModel.id);
    this.listTeamAccount.forEach(element => {
      this.listIdAccount.push(element.accountId);
    });
  }
}
