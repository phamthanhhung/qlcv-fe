import { UserService } from './../../services/user.services';
import { TeamMemberComponent } from './team-member/team-member.component';
import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { TeamService } from 'src/app/services/team.service';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { TeamIoComponent } from './team-io/team-io.component';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {


  displayedColumns: string[] = ['stt', 'name'  ,'id_leader', 'action'];
  dataSource: any;

  listTeam: any = [];
  public listAccount: any = [];
  // dialog
  public mDialog: any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private teamService: TeamService,
    private imDialog: MatDialog,
    public imDialogService: MatdialogService,
    public commonService: CommonServiceShared,
    public userService: UserService
  ) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  async ngOnInit() {
    await this.getTeam();
    await this.getListAccount();

  }
  async getTeam() {
    this.teamService.getAll().subscribe(res => {

      var data = res.map((item, index) => {
        item.stt = index + 1;
      });
      this.listTeam = res;
      this.dataSource = new MatTableDataSource(this.listTeam);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  public async getListAccount() {
    this.listAccount = await this.userService.getFetchAccount();
  }
  addTeam() {
    this.showPopup();
  }

  editTeam(item) {
    item.purpose = "edit";

    this.showPopup(item);
  }

  deleteTeam(item) {
    const dialogRef = this.commonService.confirmDeleteDiaLogService(
      "Tên phòng ban/nhóm:",
      item.name
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.teamService
          .delete(item.id).subscribe(
            () => this.getTeam(),
            (error: HttpErrorResponse) => {
              this.commonService.showError(error);
            },
            () => this.commonService.showeNotiResult("Đã xóa thành công: " + item.name, 2000)
          );
      }
    });
  }

  addMember(item) {
    this.showPopupMember(item);
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  /**
   * Mở popup io
   */
  public showPopup(data?) {

    this.mDialog.setDialog(
      this,
      TeamIoComponent,
      "",
      "",
      data,
      "40%",
      "25vh",
      1
    );
    this.mDialog.open();
  }

  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  doFunction(methodName) {
    this[methodName]();
  }

  public showPopupMember(data?) {

    this.mDialog.setDialog(
      this,
      TeamMemberComponent,
      "",
      "",
      data,
      "60%",
      "90vh",
      1
    );
    this.mDialog.open();
  }

}
