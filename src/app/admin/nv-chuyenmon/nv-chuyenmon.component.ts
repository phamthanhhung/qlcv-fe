import { CategoryService } from './../../services/category.service';
import { RoleConstants } from 'src/app/shared/constants';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from 'src/app/services/project.service';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.services';
import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { NvChuyenmonIoComponent } from './nv-chuyenmon-io/nv-chuyenmon-io.component';

@Component({
  selector: 'app-nv-chuyenmon',
  templateUrl: './nv-chuyenmon.component.html',
  styleUrls: ['./nv-chuyenmon.component.css']
})
export class NvChuyenmonComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'team', 'linhvuc', 'leader', 'name', 'start', 'end', 'action'];
  dataSource: any;

  listDuan: any = [];
  listTeam: any = [];
  listAccount: any = [];
  listCategory: any;
  listGiamdoc: any;
  // dialog
  public mDialog: any;
  public currentAccount: any;

  public selectedTeam:any;
  public selectedBangiamdoc:any;
  public menu:any;

  public checkRole = false;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private imDialog: MatDialog,
    public imDialogService: MatdialogService,
    public commonService: CommonServiceShared,
    public projectService: ProjectService,
    private router: Router,
    private teamService: TeamService,
    private userService: UserService,
    private categoryService: CategoryService,
    private route: ActivatedRoute
  ) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
    this.route.paramMap.subscribe(params => {
      this.ngOnInit();
     });
  }

  async ngOnInit() {
    this.currentAccount = JSON.parse(localStorage.getItem('currentAcc'));
    if(RoleConstants.NHOMROLEGIAMDOC.includes(this.currentAccount.role) || RoleConstants.PHUTRACHTONGHOP == this.currentAccount.role) {
      this.checkRole = true;
    }
    var getUrl = this.route.snapshot.url;
    if (getUrl.length > 1) {
      this.menu = +getUrl[1].path;
    }
    await this.getAll();
    await this.getTeam();
    await this.getListAccount();
    await this.getCategory();
    await this.getListGiamdoc();
  }

  async filter(teamId) {
    this.selectedTeam = teamId;
    var data = await this.listDuan.filter(a => a.idTeam == teamId && (this.selectedBangiamdoc == "" || a.idGiamdoc == this.selectedBangiamdoc) );

    data.map((item, index) => {
      item.stt = index + 1;
    });
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  async getAll() {
    this.selectedBangiamdoc = "";
    if (!RoleConstants.NHOMROLEGIAMDOC.includes(this.currentAccount.role)) {
      this.projectService.getNvChuyenMon(this.currentAccount.id,this.menu).subscribe(res => {
        if(this.route.snapshot.url[1].parameters.type == '0') {
          res = res.filter(a => a.idGiamdoc == this.currentAccount.id );
          this.selectedBangiamdoc = this.currentAccount.id;
        }
        var data = res.map((item, index) => {
          item.stt = index + 1;
        });
        this.listDuan = res;
        this.dataSource = new MatTableDataSource(this.listDuan);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
    } else {
      this.projectService.getNvChuyenMon("", this.menu).subscribe(res => {
        if(this.route.snapshot.url[1].parameters.type == '0') {
          res = res.filter(a => a.idGiamdoc == this.currentAccount.id );
          this.selectedBangiamdoc = this.currentAccount.id;
        }
        var data = res.map((item, index) => {
          item.stt = index + 1;
        });
        this.listDuan = res;
        this.dataSource = new MatTableDataSource(this.listDuan);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
    }


  }
  // Lấy danh sách lĩnh vực
  async getCategory() {
    this.listCategory = await this.categoryService.getFetchCategory();
  }
  public async getTeam() {
    this.listTeam = await this.teamService.getFetch();
  }

  public async getListAccount() {
    this.listAccount = await this.userService.getFetchAccount();
  }
  // danh sách tài khoản là giám đóc/ phó giám đốc
  public async getListGiamdoc() {
    this.listGiamdoc = await this.userService.getAccountByRole(RoleConstants.NHOMONLYGIAMDOC);
  }
  add() {
    this.showPopup();
  }

  edit(item) {
    item.purpose = "edit";
    this.showPopup(item);
  }

  delete(item) {
    const dialogRef = this.commonService.confirmDeleteDiaLogService(
      "Tên nhiệm vụ:",
      item.name
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.projectService
          .delete(item.id).subscribe(
            () => this.getAll(),
            (error: HttpErrorResponse) => {
              this.commonService.showError(error);
            },
            () => this.commonService.showeNotiResult("Đã xóa thành công: " + item.name, 2000)
          );
      }
    });
  }

  addTask(item) {
    this.router.navigateByUrl('/admin/task?projectid=' + item.id);
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  async filterBGD() {
    if(this.selectedTeam) {
      this.filter(this.selectedTeam);
    } else {
      var data = await this.listDuan.filter(a => (this.selectedBangiamdoc == "" || a.idGiamdoc == this.selectedBangiamdoc) );

      data.map((item, index) => {
        item.stt = index + 1;
      });
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }
  }
  /**
   * Mở popup io
   */
  public showPopup(data?) {
    this.mDialog.setDialog(
      this,
      NvChuyenmonIoComponent,
      "",
      "",
      data,
      "50%",
      "50vh",
      1
    );
    this.mDialog.open();
  }

  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  doFunction(methodName) {
    this[methodName]();
  }


}
