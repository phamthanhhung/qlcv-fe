import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NvChuyenmonIoComponent } from './nv-chuyenmon-io.component';

describe('NvChuyenmonIoComponent', () => {
  let component: NvChuyenmonIoComponent;
  let fixture: ComponentFixture<NvChuyenmonIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NvChuyenmonIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NvChuyenmonIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
