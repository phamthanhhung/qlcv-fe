import { CategoryService } from './../../../services/category.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RoleService } from 'src/app/services/role.service';
import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { ProgressService } from 'src/app/shared/services/progress-http.service';
import { validationAllErrorMessagesService, displayFieldCssService } from 'src/app/shared/validators/validatorService';
import { RoleIoComponent } from '../../roles/role-io/role-io.component';

@Component({
  selector: 'app-linh-vuc-io',
  templateUrl: './linh-vuc-io.component.html',
  styleUrls: ['./linh-vuc-io.component.css']
})
export class LinhVucIoComponent implements OnInit {

  IOForm: FormGroup;
  public inputModel: any;
  public editMode: boolean;
  public purpose: string;
  public obj: any;
  dataModel: any;
  // error message
  validationErrorMessages = {
    name: { required: "Tên lĩnh vực không được để trống!" },
  };

  // form errors
  formErrors = {
    name: "",
  };

  // ctor
  constructor(
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public imDialogService: MatdialogService,
    private categoryService: CategoryService,
    private commonService: CommonServiceShared,
    public dialogRef: MatDialogRef<RoleIoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,

  ) { }

  // onInit
  ngOnInit() {
    if (this.data.model && this.data.model.purpose) {
      this.purpose = this.data.model.purpose;
      this.dataModel = this.data.model
    }
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }

  // config Form use add or update
  bindingConfigAddOrUpdate() {
    // check edit
    if (this.dataModel) {
      this.IOForm.setValue({
        name: this.dataModel.name,
      });
    }
  }

  // config input validation form
  bindingConfigValidation() {

    this.IOForm = this.formBuilder.group({
      name: ["", Validators.required],
    });
  }


  // on Submit
  public onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.IOForm.valid === true && !this.purpose) {
      this.inputModel = this.IOForm.value;
      this.categoryService.insert(this.inputModel).subscribe(
        res => this.imDialogService.doParentFunction("getCategory"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Thêm mới lĩnh vực thành công!",
            2000
          )
      );
      this.imDialogService.doParentFunction("closeMatDialog");
    } if (this.IOForm.valid === true && this.purpose) {
      this.inputModel = this.IOForm.value;
      this.inputModel.id = this.dataModel.id;
      this.categoryService.update(this.inputModel).subscribe(
        res => this.imDialogService.doParentFunction("getCategory"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Cập nhật lĩnh vực thành công!",
            2000
          )
      );
      this.imDialogService.doParentFunction("closeMatDialog");
    }


  }

  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.IOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // close sidebar
  public closeIOSidebar() {
    this.imDialogService.doParentFunction("closeMatDialog");
  }

}
