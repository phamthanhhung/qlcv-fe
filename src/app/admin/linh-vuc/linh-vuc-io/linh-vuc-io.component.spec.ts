import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinhVucIoComponent } from './linh-vuc-io.component';

describe('LinhVucIoComponent', () => {
  let component: LinhVucIoComponent;
  let fixture: ComponentFixture<LinhVucIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinhVucIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinhVucIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
