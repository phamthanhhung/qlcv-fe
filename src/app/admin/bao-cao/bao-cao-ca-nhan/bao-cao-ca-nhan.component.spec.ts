import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaoCaoCaNhanComponent } from './bao-cao-ca-nhan.component';

describe('BaoCaoCaNhanComponent', () => {
  let component: BaoCaoCaNhanComponent;
  let fixture: ComponentFixture<BaoCaoCaNhanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaoCaoCaNhanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaoCaoCaNhanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
