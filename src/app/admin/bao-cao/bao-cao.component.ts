import { CommonServiceShared } from './../../shared/services/common-services';
import { ProjectService } from './../../services/project.service';
import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { IssueService } from 'src/app/services/issue.service';
import { TeamAccountService } from 'src/app/services/team-account.service';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.services';
import { BaoCaoIssue } from './bao-cao-ca-nhan/bao-cao-ca-nhan.component';
import { RoleConstants } from 'src/app/shared/constants';

@Component({
  selector: 'app-bao-cao',
  templateUrl: './bao-cao.component.html',
  styleUrls: ['./bao-cao.component.css']
})
export class BaoCaoComponent implements OnInit {
  @ViewChild('TABLE', { static: true }) table: ElementRef;
  displayedColumns: string[] = ['position', 'name', 'cao', 'dat', 'thap', 'chua-dam-bao', 'tong-so-congviec', 'vuot', 'dung', 'cham', 'khach-quan', 'dieu-chinh', 'chu-quan',
    'chaphanh-ot', 'khong-chaphanh-ot', 'chaphanh-ttbc', 'khong-chaphanh-ttbc', 'ghi-chu'];
  dataSource: any;
  type: any;
  // ngModel
  public projectId: any;
  public timeFrom: any;
  public timeTo: any;

  public listDuan: any;
  public listIssue: any = [];
  public currentUser: any;
  public listAccount: any = [];
  RoleConstants = RoleConstants;
  constructor(
    public teamService: TeamService,
    public teamAccService: TeamAccountService,
    public datePipe: DatePipe,
    private issueService: IssueService,
    private userService: UserService,
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private commonService: CommonServiceShared
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentAcc'));
    this.type = this.route.snapshot.paramMap.get('type');
    this.getDuan();
    this.getListAccount();
  }

  // export table ra file excel
  ExportTOExcel() {
    this.commonService.ExportTOExcel(this.table, 'bao-cao-nhiem-vu');

  }
  // lấy danh sách dự án
  public async getDuan() {
    if (this.currentUser.role == RoleConstants.SU || this.currentUser.role == RoleConstants.GIAMDOC || this.currentUser.role == RoleConstants.PHUTRACHTONGHOP) {
      if (this.type == 0)
        this.listDuan = await this.projectService.getFetchNvChuyenMon('');
      if (this.type == 1)
        this.listDuan = await this.projectService.getFetchDuan('');
    } else {
      if (this.type == 0)
        this.listDuan = await this.projectService.getFetchNvChuyenMon(this.currentUser.id);
      if (this.type == 1)
        this.listDuan = await this.projectService.getFetchDuan(this.currentUser.id);
    }

  }

  // danh sách acc
  public async getListAccount() {
    this.listAccount = await this.userService.getFetchAccount();
  }

  // báo cáo
  report() {
    this.listIssue = [];
    let dateFrom = "";
    let dateTo = "";

    if (this.timeFrom && this.timeTo) {
      dateFrom = this.datePipe.transform(
        this.timeFrom,
        "MM-dd-yyyy"
      );

      dateTo = this.datePipe.transform(
        this.timeTo,
        "MM-dd-yyyy"
      );
    }


    this.issueService.getReportByIdProject(this.projectId, dateFrom, dateTo).subscribe(res => {
      res.map((item, index) => {
        item.stt = index + 1;
      });
      if (res.length > 0) {
        this.commonService.showeNotiResult("Lấy dữ liệu báo cáo thành công !", 2000);
      } else {
        this.commonService.showeNotiResult("Không có công việc cho nhiệm vụ/dự án này !", 2000);
      }

      this.dataSource = new MatTableDataSource(res);
    });
  }

}
