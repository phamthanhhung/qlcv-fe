import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaoCaoTeamComponent } from './bao-cao-team.component';

describe('BaoCaoTeamComponent', () => {
  let component: BaoCaoTeamComponent;
  let fixture: ComponentFixture<BaoCaoTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaoCaoTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaoCaoTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
