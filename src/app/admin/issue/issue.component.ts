import { RoleConstants } from 'src/app/shared/constants';
import { SettingService } from './../../services/setting.service';
import { DetailTaskComponent } from './../detail-task/detail-task.component';
import { async } from '@angular/core/testing';
import { CommonServiceShared } from './../../shared/services/common-services';
import { IssueIoComponent } from './issue-io/issue-io.component';
import { UserService } from './../../services/user.services';
import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { IssueService } from 'src/app/services/issue.service';
import { ProjectService } from 'src/app/services/project.service';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { TaskIoComponent } from '../task-subtask/task-io/task-io.component';
import { HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { tap } from 'rxjs/operators';
import { MenuIssueService } from 'src/app/services/menu-issues.service';
import { TeamService } from 'src/app/services/team.service';
import { TeamAccountService } from 'src/app/services/team-account.service';
import { IssueExpandDuedateComponent } from './issue-expand-duedate/issue-expand-duedate.component';

@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.css']
})
export class IssueComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['stt', 'reporter_id', 'idProject', 'title', 'assign_id', 'due_date', 'confirm_date', 'num_extend', 'action'];
  dataSource: any;

  // dialog
  public mDialog: any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public idProject: any;
  public project: any = {};
  public listIssue: any = [];
  public selectedIssue: any = {};
  public currentUser: any;
  public listProject: any;
  public listAccount: any = [];
  public listTrangthai: any;
  public type: any;
  public mode: any;
  // các biến lọc
  public trangthai: any = "";
  public timeFrom: any;
  public timeTo: any;
  public isRisingWork: any;

  //
  public listTeam: any;
  public selectedTeam: any;
  //pagingData
  public countPagingData: any = 0;

  public RoleConstants = RoleConstants;
  //count issues data
  public counts:any;
  public countAllIssue: any;

  constructor(private route: ActivatedRoute, private projectService: ProjectService,
    private issueService: IssueService,
    private imDialog: MatDialog,
    public imDialogService: MatdialogService,
    private userService: UserService,
    private commonService: CommonServiceShared,
    private settingService: SettingService,
    public datePipe: DatePipe,
    public menuIssueService: MenuIssueService,
    public teamService: TeamService,
    public teamAccService: TeamAccountService,
    private cdr: ChangeDetectorRef,
    private router: Router) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
    
  }
  ngAfterContentChecked(): void {
    this.cdr.detectChanges();
 }  
  async ngOnInit() {
    this.isRisingWork = "";
    this.selectedTeam = "";
    var getUrl = this.route.snapshot.url;
    if (getUrl.length > 1) {
      this.trangthai = this.route.snapshot.paramMap.get('status');
      this.mode = 'viewfilter';
    } else {
      this.mode = 'viewall'
    }
    if(this.trangthai != undefined) {

      this.route.params.forEach(params => {
        this.trangthai = this.route.snapshot.paramMap.get('status');

        this.filterbyTeam(this.selectedTeam);

      })
    }

    this.type = this.route.snapshot.paramMap.get('type');

    await this.getProfile();
    await this.getTeam();
    this.filterbyTeam(this.selectedTeam);
    this.getDuan();
    this.getListAccount();
    this.getTrangthai();

  }

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        tap(() => this.filter(this.paginator.pageSize, this.paginator.pageIndex + 1, this.selectedTeam))
      )
      .subscribe();
  }

  // Kiểm tra công việc có quá hạn không // có thì hiển thị màu đỏ
  public checkOutDateTask(element) {
    if(element.status != 0 && element.dueDate != null && new Date(element.dueDate) < new Date()) {
      return true;
    } else return false;
  }

  // Kiểm tra current user có phải coop của công việc không
  public checkCoop(element: any) {
    if (element.coopId.includes(this.currentUser.id)) {
      return true;
    }
    return false;
  }

  // Hiển thị nút xóa với role GĐ, SU; hoặc là người tạo việc
  public checkDeleteButton(element) {
    if(element.status == 0) {
      return true;
    }
    if(RoleConstants.NHOMROLEGIAMDOC.includes(this.currentUser.role)) {
      return false;
    } 
    else 
    {
      if(element.idCreator == this.currentUser.id) {
        return false;
      }
      return true;
    }
  } 

  // Hiển thị nút sửa với role GĐ TP PTP
  public checkEditButton(element) {
    if(element.status == 0) {
      return true;
    }
    if(RoleConstants.NHOMROLEGIAMDOC.includes(this.currentUser.role)) {
      return false;
    } 
    else 
    {
      if(RoleConstants.NHOMCANEDITISSUES.includes(this.currentUser.role)) {
        return false;
      }
      return true;
    }
  } 

  // danh sách phòng ban
  public async getTeam() {
    this.listTeam = await this.teamService.getFetch();
  }

  // filter theo phòng ban
  async filterbyTeam(teamId) {
    this.selectedTeam = teamId;
    this.paginator.pageIndex = 0;
    this.filter(this.paginator.pageSize, 1, teamId);
  }

  // lấy dữ liệu
  async filter(pageSize?, pageIndex?, idTeam = "") {
    idTeam = this.selectedTeam;
    if (!pageSize && !pageIndex) {
      pageSize = this.paginator.pageSize;
      pageIndex = this.paginator.pageIndex + 1;
    }
    let dateFrom = "";
    let dateTo = "";

    if (this.timeFrom && this.timeTo) {
      dateFrom = this.datePipe.transform(
        this.timeFrom,
        "MM-dd-yyyy"
      );

      dateTo = this.datePipe.transform(
        this.timeTo,
        "MM-dd-yyyy"
      );

    }
    let accountId = "";
    if (!RoleConstants.NHOMROLEGIAMDOC.includes(this.currentUser.role)) {
      accountId = this.currentUser.id;
    }

    // Trường hợp Phó phòng hiển thị tương tự Trưởng phòng nhưng ko cho edit ( không áp dụng với việc cần xử lý và việc đã giao)
    // change 8/8/2022: cho phép phó phòng edit
    if (this.currentUser.role == RoleConstants.PHOTRUONGPHONG && this.trangthai == 0) {
      // lấy idteam
      let teamAcc = await this.teamAccService.getbyAccountId(this.currentUser.id);

      // lấy idtruongphong
      let team = this.listTeam.filter(a => a.id == teamAcc.teamId)[0];

      // gán accountId = id trưởng phòng
      accountId = team.idLeader;
    }

    // LẤY CÔNG VIỆC THEO MENU
    if (this.mode === 'viewfilter') {

      let menu = this.trangthai;
      this.menuIssueService.getByMenu(accountId, menu, dateFrom, dateTo, this.isRisingWork, idTeam, pageSize, pageIndex).subscribe(
        res => {
          let stt = 0;
          this.countPagingData = res.totalItems;
          this.listIssue = res.items;
          this.counts = res.countTeams;
          this.countAllIssue = res.countAll;

          this.listIssue.map((item) => {
            if (!item.parentId) {
              item.stt = stt + (pageIndex - 1) * pageSize + 1;
              stt++;
            }

          });
          this.dataSource = new MatTableDataSource(this.listIssue);
          this.dataSource.sort = this.sort;
          this.commonService.showeNotiResult("Đã lấy dữ liệu thành công!", 2000);
        },
        (error: HttpErrorResponse) => {
          this.commonService.showeNotiResult(error.message, 2000);
        });
    }

    // LẤY TẤT CẢ CÔNG VIỆC KHÔNG FILTER
    if (this.mode === 'viewall') {
      let menu = 0;
      this.menuIssueService.getByMenu(accountId, menu, dateFrom, dateTo, this.isRisingWork, idTeam, pageSize, pageIndex).subscribe(
        res => {
          let stt = 0;
          this.countPagingData = res.totalItems;
          this.listIssue = res.items;
          this.counts = res.countTeams;
          this.countAllIssue = res.countAll;
          
          this.listIssue.map((item) => {
            if (!item.parentId) {
              item.stt = stt + (pageIndex - 1) * pageSize + 1;
              stt++;
            }
          });
          this.dataSource = new MatTableDataSource(this.listIssue);
          this.dataSource.sort = this.sort;
          this.commonService.showeNotiResult("Đã lấy dữ liệu thành công!", 2000);
        },
        (error: HttpErrorResponse) => {
          this.commonService.showeNotiResult(error.message, 2000);
        });
    }

  }
  // lấy json trạng thái
  async getTrangthai() {
    this.listTrangthai = await this.settingService.getFetchSetting('0', 'trangthai');
  }
  // lấy user hiện tại
  public async getProfile() {
    this.currentUser = await this.userService.getFetchProfile();
    console.log(this.currentUser);
  }

  public async getDuan() {
    this.listProject = await this.projectService.getProjectds();
  }
  public async getListAccount() {
    this.listAccount = await this.userService.getFetchAccount();
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  addTask() {
    var data = {
      purpose: "add",
      idProject: this.idProject,
      idLeader: this.project.idLeader,
      type: this.type
    }
    this.showPopup(data);
  }

  // sửa
  editTeam(item) {
    item.purpose = "edit";
    item.type = this.type
    this.showPopup(item);
  }
  expandDueDate(item) {
    this.mDialog.setDialog(
      this,
      IssueExpandDuedateComponent,
      "",
      "",
      item,
      "50%",
      "30vh",
      1
    );
    this.mDialog.open();
  }
  // xem chi tiết
  view(item) {
    this.mDialog.setDialog(
      this,
      DetailTaskComponent,
      "",
      "",
      item,
      "60%",
      "90vh",
      1
    );
    this.mDialog.open();
  }

  // thêm subtask
  addSubTask(item) {
    var data = {
      purpose: "add",
      idProject: item.idProject,
      type: this.type,
      addSubtask: true,
      parentId: item.id
    }
    this.showPopup(data);
  }

  // xóa
  deleteTeam(item) {
    const dialogRef = this.commonService.confirmDeleteDiaLogService(
      "Tên công việc:",
      item.title
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.issueService
          .delete(item.id).subscribe(
            () => this.filter(),
            (error: HttpErrorResponse) => {
              this.commonService.showError(error);
            },
            () => this.commonService.showeNotiResult("Đã xóa thành công: " + item.title, 2000)
          );
      }
    });
  }
  /**
 * Mở popup io
 */
  public showPopup(data?) {
    this.mDialog.setDialog(
      this,
      IssueIoComponent,
      "",
      "",
      data,
      "50%",
      "60vh",
      1
    );
    this.mDialog.open();
  }

  /**
 * Hàm đóng mat dialog
 */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  doFunction(methodName) {
    this[methodName]();
  }
}
