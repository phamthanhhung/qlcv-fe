import { RoleConstants } from 'src/app/shared/constants';
import { async } from '@angular/core/testing';
import { TeamAccountService } from 'src/app/services/team-account.service';
import { SettingService } from './../../../services/setting.service';
import { HttpErrorResponse, HttpEventType, HttpResponse } from '@angular/common/http';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IssueService } from 'src/app/services/issue.service';
import { ProjectService } from 'src/app/services/project.service';
import { UserService } from 'src/app/services/user.services';
import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { ProgressService } from 'src/app/shared/services/progress-http.service';
import { validationAllErrorMessagesService, displayFieldCssService } from 'src/app/shared/validators/validatorService';
import { RoleIoComponent } from '../../roles/role-io/role-io.component';
import { DatePipe, formatDate } from '@angular/common';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-issue-expand-duedate',
  templateUrl: './issue-expand-duedate.component.html',
  styleUrls: ['./issue-expand-duedate.component.css']
})
export class IssueExpandDuedateComponent implements OnInit {

  RoleConstants = RoleConstants;
  IOForm: FormGroup;
  public inputModel: any;
  public editMode: boolean;
  public purpose: string;
  public mode: string = "hasProject";
  public obj: any;
  dataModel: any;
  listDuan: any;
  listUser: any;
  listNguoigiaoviec: any;
  listNguoinhanviec: any;
  listTrangthai: any;
  listMucdo: any;
  listTeam: any;

  projectDueDate: any;
  checkIssueDueDate: boolean = true;

  percentDone = 0;
  uploadSuccess = false;

  public fileUrl: any = {
    url: ""
  };
  public currentUser: any = {};
  public checkDisableTittle = false;
  // error message
  validationErrorMessages = {
    title: { required: "Trường bắt buộc nhập!" },
    idProject: { required: "Trường bắt buộc nhập!" },
    assignId: { required: "Trường bắt buộc nhập!" },
    dueDate: { required: "Trường bắt buộc nhập!" },
    priority: { required: "Trường bắt buộc nhập!" },
    reporterId: { required: "Trường bắt buộc nhập!" },
    status: { required: "Trường bắt buộc nhập!" },
    startedDate: { required: "Trường bắt buộc nhập!" },
  };

  // form errors
  formErrors = {
    title: "",
    idProject: "",
    assignId: "",
    dueDate: "",
    priority: "",
    reporterId: "",
    status: "",
    startedDate: ""
  };

  // ctor
  constructor(
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public imDialogService: MatdialogService,
    private projectService: ProjectService,
    private commonService: CommonServiceShared,
    public dialogRef: MatDialogRef<RoleIoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public issueService: IssueService,
    public userService: UserService,
    public settingService: SettingService,
    public teamAccountService: TeamAccountService,
    public teamService: TeamService,
    private cdr: ChangeDetectorRef
  ) { }

  // onInit
  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentAcc'));
    if (this.data.model) {
      this.dataModel = this.data.model
    }
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }

  ngAfterContentChecked(): void {
    this.cdr.detectChanges();
 }  

  // config Form use add or update
  bindingConfigAddOrUpdate() {
    console.log(this.dataModel);
    if (this.dataModel) {
      this.IOForm.setValue({
        dueDate: this.dataModel.dueDate,
      });
    }
  }


  // config input validation form
  bindingConfigValidation() {
    this.IOForm = this.formBuilder.group({
      dueDate: [""],
    });
  }
  // on Submit
  public onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    //this.checkIssueDueDate = this.checkDueDate(this.projectDueDate, this.IOForm.value.dueDate);
    if (this.IOForm.valid === true && this.checkIssueDueDate == true) {
      this.inputModel = this.IOForm.value;
      this.inputModel.id = this.dataModel.id;
      this.issueService.updateDueDate(this.inputModel).subscribe(
        res => this.imDialogService.doParentFunction("filter"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Cập nhật công việc thành công!",
            2000
          )
      );
      this.imDialogService.doParentFunction("closeMatDialog");
    }


  }

  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.IOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // Kiểm tra dueDate công việc so với dueDate nhiệm vụ
  public checkDueDate(projectDueDate, issueDueDate) {
    if (!projectDueDate || !issueDueDate) {
      return true;
    } else {
      projectDueDate = formatDate(projectDueDate, 'yyyy-MM-dd', 'en_US');

      issueDueDate = formatDate(issueDueDate, 'yyyy-MM-dd', 'en_US');

      if (projectDueDate >= issueDueDate) {
        return true;
      } else {
        return false;
      }
    }
  }
  // close sidebar
  public closeIOSidebar() {
    this.imDialogService.doParentFunction("closeMatDialog");
  }

}

