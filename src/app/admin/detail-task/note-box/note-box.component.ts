import { SettingService } from './../../../services/setting.service';
import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { IssueService } from 'src/app/services/issue.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-note-box',
  templateUrl: './note-box.component.html',
  styleUrls: ['./note-box.component.css']
})
export class NoteBoxComponent implements OnInit {
  dataModel: any;
  note: any = "";
  reason: any = 0;
  public inputModel: any;
  public listReasonLate:any;

  // check có mở note box hay không
  public openNote: any;

  constructor(public imDialogService: MatdialogService,
    public dialogRef: MatDialogRef<NoteBoxComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public issueService: IssueService,
    public commonService: CommonServiceShared,
    public settingService: SettingService) { }

  ngOnInit() {
    this.dataModel = this.data.model;
    this.openNote = this.data.model.openNote;
    this.getReasonSeting();
    if(this.dataModel.model.reasonLate) {
      this.reason = this.dataModel.model.reasonLate;
    } else {
      this.reason = 0;
    }
  }

  async getReasonSeting() {
    this.listReasonLate = await this.settingService.getFetchSetting(0, 'reason_late');
  }

  chooseReason(item) {
    this.reason = item;
  }

  closeDialog() {
    this.dialogRef.close();
    this.imDialogService.doParentFunction("resetButtonToggle");
  }

  submit() {
    this.inputModel = this.dataModel.model;
    this.inputModel[this.dataModel.key] = this.dataModel.value;
    this.inputModel.note = this.note;
    if(this.dataModel.isLate) {
      this.inputModel.reasonLate = this.reason;
    } else {
      this.inputModel.reasonLate = -1;
    }
    
    if(this.dataModel.isConfirmAction == true) {
      this.inputModel.status = 0;
    }
    this.inputModel.userActionId = JSON.parse(localStorage.getItem('currentAcc')).id;

    // khi trạng thái là đổi sang hoàn thành, tự động đổi ngày kết thúc là ngày hiện tại
    if(this.dataModel.key == 'status' && this.dataModel.value == 0) {
      this.inputModel.confirmDate = new Date();
    }
    // tự động đổi lại ngày hoàn thành khi trạng thái là chưa hoàn thành
    if(this.dataModel.key == 'status' && this.dataModel.value == 1) {
      this.inputModel.confirmDate = null;
    }
    this.issueService.update(this.inputModel).subscribe(
      res => this.imDialogService.doParentFunction("ngOnInit"),
      (error: HttpErrorResponse) => {
        this.commonService.showError(error);
      },
      () =>
        this.commonService.showeNotiResult(
          "Cập nhật công việc thành công!",
          2000
        )
    );
    this.dialogRef.close();
  }

}
