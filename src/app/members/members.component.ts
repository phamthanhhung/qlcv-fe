import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { RoleService } from './../services/role.service';
import { map } from 'rxjs/operators';
import { UserService } from 'src/app/services/user.services';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatdialogService } from '../shared/services/mat-dialog.service';
import { MemberIoComponent } from './member-io/member-io.component';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'fullName', 'sodienthoai', 'userName', 'role', 'action'];
  dataSource: any;

  listMember: any = [];
  listRole: any = [];
  currentAccount: any;
  // dialog
  public mDialog: any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private userService: UserService,
    private imDialog: MatDialog,
    public imDialogService: MatdialogService,
    private roleService: RoleService,
    private commonService: CommonServiceShared
  ) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  async ngOnInit() {
    this.currentAccount = JSON.parse(localStorage.getItem('currentAcc'));
    await this.getMember();
    await this.getRole();
  }
  async getMember() {
    this.userService.getAccount().subscribe(res => {

      this.listMember = res.filter(a => a.userName !== 'su');
      this.listMember.map(async (item, index) => {
        item.stt = index + 1;
      });
      this.dataSource = new MatTableDataSource(this.listMember);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  public async getRole() {
    this.listRole = await this.roleService.getFetchRole();
  }

  addMember() {
    this.showPopup();
  }

  editMember(item) {
    item.purpose = "edit";
    this.showPopup(item);
  }

  
  deleteMember(item) {
    const dialogRef = this.commonService.confirmDeleteDiaLogService(
      "Tên thành viên:",
      item.fullName
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.userService
          .deletetUser(item.id).subscribe(
          () => this.getMember(),
          (error: HttpErrorResponse) => {
            this.commonService.showError(error);
          },
          () => this.commonService.showeNotiResult("Đã xóa thành công: " + item.fullName, 2000)
        );
      }
    });
  }

  // Khôi phục mật khẩu gốc
  public resetPassword(user) {

     const dialogRef = this.commonService.confirmResetPassword(
      user.fullName
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.userService.resetPassword(user.idUser).subscribe(
          () => this.getMember(),
          (error: HttpErrorResponse) => {
            this.commonService.showError(error);
          },
          () => this.commonService.showeNotiResult("Đã khôi phục mật khẩu tài khoản: " + user.fullName, 2000)
        );
      }
    });
  }

  /**
   * filter
   */
  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  /**
   * Mở popup
   */
  public showPopup(data?) {

    this.mDialog.setDialog(
      this,
      MemberIoComponent,
      "",
      "",
      data,
      "40%",
      "43vh",
      1
    );
    this.mDialog.open();
  }

  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  doFunction(methodName) {
    this[methodName]();
  }
}
