import { RoleService } from './../../services/role.service';
import { CommonServiceShared } from './../../shared/services/common-services';
import { UserService } from 'src/app/services/user.services';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { ProgressService } from 'src/app/shared/services/progress-http.service';
import { displayFieldCssService, validationAllErrorMessagesService } from 'src/app/shared/validators/validatorService';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-member-io',
  templateUrl: './member-io.component.html',
  styleUrls: ['./member-io.component.css']
})
export class MemberIoComponent implements OnInit {

  IOForm: FormGroup;
  public inputModel: any;
  public editMode: boolean;
  public purpose: string;
  public obj: any;
  dataModel: any;
  listRole: any;
  // error message
  validationErrorMessages = {
    userName: { required: "Tên đăng nhập không được để trống!" },
    fullName: { required: "Họ và Tên không được để trống!" },
    password: { required: "Mật khẩu không được để trống!" },
    role: { required: "Vai trò không được để trống!" }
  };

  // form errors
  formErrors = {
    userName: "",
    fullName: "",
    password: "",
    role: ""
  };

  // ctor
  constructor(
    public progressService: ProgressService,
    public roleService: RoleService,
    private formBuilder: FormBuilder,
    public imDialogService: MatdialogService,
    private userService: UserService,
    private commonService: CommonServiceShared,
    public dialogRef: MatDialogRef<MemberIoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,

  ) { }

  // onInit
  async ngOnInit() {
    if (this.data.model && this.data.model.purpose) {
      this.purpose = this.data.model.purpose;
      this.dataModel = this.data.model
    }

    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }

  // config Form use add or update
  bindingConfigAddOrUpdate() {
   this.getListRole();
    // check edit
    if (this.dataModel) {
      this.IOForm.setValue({
        userName: this.dataModel.userName,
        // password: this.dataModel.password,
        email: this.dataModel.email,
        sodienthoai: this.dataModel.sodienthoai,
        role: this.dataModel.role,
        fullName: this.dataModel.fullName
      });
    }
  }

  // config input validation form
  bindingConfigValidation() {
    this.getListRole();
    if (this.purpose === "edit") {
      this.IOForm = this.formBuilder.group({
        userName: ["", Validators.required],
        email: [""],
        sodienthoai: [""],
        fullName: ["", Validators.required],
        role: ["", Validators.required]
      });
    } else {
      this.IOForm = this.formBuilder.group({
        userName: ["", Validators.required],
        email: [""],
        sodienthoai: [""],
        password: ["", Validators.required],
        fullName: ["", Validators.required],
        role: ["", Validators.required]
      });
    }

  }

  // on Submit
  public onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.IOForm.valid === true && !this.purpose) {
      this.inputModel = this.IOForm.value;
      this.userService.insertUser(this.inputModel).subscribe(
        res => this.imDialogService.doParentFunction("getMember"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Thêm mới thành viên thành công!",
            2000
          )
      );
      this.imDialogService.doParentFunction("closeMatDialog");
    }
    if (this.IOForm.valid === true && this.purpose === "edit") {
      this.inputModel = this.IOForm.value;
      this.inputModel.id = this.dataModel.id;
      this.inputModel.idUser = this.dataModel.idUser;
      this.userService.updatetUser(this.inputModel).subscribe(
        res => this.imDialogService.doParentFunction("getMember"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Cập nhật thành viên thành công!",
            2000
          )
      );
      this.imDialogService.doParentFunction("closeMatDialog");
    }

  }

  // add or update continue
  public addOrUpdateContinue(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.IOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }

  // add or update
  private addOrUpdate(operMode: string) {

  }

  // on form reset
  public onFormReset() {
    this.IOForm.reset();
  }
  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.IOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }


  public async getListRole() {
    this.listRole = await this.roleService.getFetchRole();
    
  }

  // close sidebar
  public closeIOSidebar() {
    this.imDialogService.doParentFunction("closeMatDialog");
  }

}
