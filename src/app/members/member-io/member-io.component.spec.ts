import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberIoComponent } from './member-io.component';

describe('MemberIoComponent', () => {
  let component: MemberIoComponent;
  let fixture: ComponentFixture<MemberIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
